﻿
using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Runtime.InteropServices;
using PCAR.Cameron.DeviceLib.Wrapper;

namespace PCAR.Cameron.DeviceDashboard
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {

    DeviceLib.Wrapper.CameronDevice cameron;

    public MainWindow()
    {
      InitializeComponent();

      //creates the reference to Cameron
      cameron = new DeviceLib.Wrapper.CameronDevice();
      cameron.CameronFrameReceived += Cameron_CameronFrameReceived;
      cameron.CameronDeviceStatusChanged += Cameron_CameronDeviceStatusChanged;

      //sets the device in the image viewer
      iLiveFeed.Device = cameron;

      //starts receiving frames and IMU data
      cameron.Start(this.iLiveFeed);
            //Console.Write("bitmapWidth:" + (uint)CameronDeviceLib.CAMERON_DEFAULT_RESOLUTION_WIDTH);
            //Console.Write("BitmapWidth:" + (uint)iLiveFeed.image.BitmapWidth);
        }

    private void Cameron_CameronFrameReceived(DeviceLib.Wrapper.CameronDevice sender, ref DeviceLib.Wrapper.CameronFrame pFrameData)
    {
      //Please update the image in the CameronFrameViewer.cs - NOT HERE

      //updates the IMU values here
      DeviceLib.Wrapper.CameronIMUSample imu = pFrameData.IMUData[0];
            this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    lbAccX.Content = imu.accelData[0];
                    lbAccY.Content = imu.accelData[1];
                    lbAccZ.Content = imu.accelData[2];
                    lbGyroX.Content = imu.gyroData[0];
                    lbGyroY.Content = imu.gyroData[1];
                    lbGyroZ.Content = imu.gyroData[2];
                    lbMagnX.Content = imu.magData[0];
                    lbMagnY.Content = imu.magData[1];
                    lbMagnZ.Content = imu.magData[2];
                }));
            //new Thread(() => {
            //    this.Dispatcher.Invoke(new Action(() =>
            //    {
            //        lbAccX.Content = imu.accelData[0];
            //        lbAccY.Content = imu.accelData[1];
            //        lbAccZ.Content = imu.accelData[2];
            //        lbGyroX.Content = imu.gyroData[0];
            //        lbGyroY.Content = imu.gyroData[1];
            //        lbGyroZ.Content = imu.gyroData[2];
            //        lbMagnX.Content = imu.magData[0];
            //        lbMagnY.Content = imu.magData[1];
            //        lbMagnZ.Content = imu.magData[2];
            //    }));
            //}).Start();
        }



        private void Cameron_CameronDeviceStatusChanged(DeviceLib.Wrapper.CameronDevice sender, bool isRunning)
    {

    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      //Closes the Cameron reference and calls for Garbagge collection
      cameron.Stop();
      DeviceLib.Wrapper.CameronDeviceLib.CloseCameron(cameron.IntPtr);
    }

    #region Info

    private void tabInfo_Loaded(object sender, RoutedEventArgs e)
    {
      //Queries for all the values
      lbDeviceName.Content = cameron.DeviceName;
      lbSerialNumber.Content = cameron.SerialNumber;
      lbFirmwareVersion.Content = cameron.FirmwareVersion;
      lbFirmwareBuild.Content = cameron.FirmwareBuild;
      lbDeviceLibVersion.Content = DeviceLib.Wrapper.CameronDeviceLib.GetLibVersion();

      System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
      FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
      lbDashboardVersion.Content = fvi.FileVersion;

      lbRuntimeVersion.Content = "NEEDS CAMERON SDK"; //get this using the CameronSDK
      
      //JUST FOR TESTING COMMUNICATION TO THE C++ LIB
      {
        Cameron.DeviceLib.Wrapper.CameronDeviceLib.SetCameronDeviceName(cameron.IntPtr, "Dummy Device Name");
        Cameron.DeviceLib.Wrapper.CameronDeviceLib.SetCameronSerialNumber(cameron.IntPtr, "Dummy Serial Number");
      }
    }

    #endregion

    #region Control

    private void tabControl_Loaded(object sender, RoutedEventArgs e)
    {
      //Queries for all the Camera values 
      lFrameSize.Content = string.Format("{0}x{1} @ {2}", cameron.Resolution.width, cameron.Resolution.height, cameron.Resolution.fps);
      slGain.Value = cameron.Gain;
      slExposure.Value = cameron.Exposure;
      chbAutoExposure.IsChecked = cameron.AutoExposure;
      chbUndistort.IsChecked = cameron.Undistort;
            //lbAccX.Content = string.Format("{0}@{1}", cameron.FrameWidth, cameron.FrameWidth);
      if (cameron.HFlip)
        cobFlip.SelectedIndex = 1;
      if (cameron.VFlip)
        cobFlip.SelectedIndex = 2;
      if (cameron.HFlip && cameron.VFlip)
        cobFlip.SelectedIndex = 3;

      //Queries for all the Display values 
      chbOnOffLeft.IsChecked = cameron.DisplayStatus[0];
      chbOnOffRight.IsChecked = cameron.DisplayStatus[1];


      slBrightnessLeft.Value = cameron.DisplayBrightness[0];
      slBrightnessRight.Value = cameron.DisplayBrightness[1];

      ////Queries for all the Status LED values
      slStatusOn.Value = cameron.LEDOnOff[0];
      slStatusOff.Value = cameron.LEDOnOff[1];
      //JUST FOR TESTING COMMUNICATION TO THE C++ LIB
      {
        cameron.Resolution = new DeviceLib.Wrapper.CameronResolutionInfo() { width = 3000, height = 5000, fps = 3123 };
        cameron.Gain = 1234;
        cameron.Exposure = 2345;
        cameron.AutoExposure = true;
        cameron.Undistort = true;
        //cameron.HFlip = true;
        //cameron.VFlip = true;
        cameron.DisplayStatus = new bool[] { true, true };
        cameron.DisplayBrightness = new byte[] {1,2,3,1, 56, 67 };
            }
        }

    private void slGain_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {

    }

    private void slExposure_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {

    }

    private void chbAutoExposure_Click(object sender, RoutedEventArgs e)
    {

    }

    private void cobFlip_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
    {
            //if (cameron != null)
            //{
            //    switch (cobFlip.SelectedIndex)
            //    {
            //        case 0: {
            //                cameron.HFlip = false;
            //                cameron.VFlip = false;
            //                break; }
            //        case 1: {
            //                cameron.HFlip = true;
            //                cameron.VFlip = false;
            //                break; }
            //        case 2:
            //            {
            //                cameron.HFlip = false;
            //                cameron.VFlip = true;
            //                break; }
            //        case 3:
            //            {
            //                cameron.HFlip = true;
            //                cameron.VFlip = true;
            //                break; }
            //        default: { Console.WriteLine("Flip Set Error");break; }
            //    }
            //}
        }

    private void chbUndistort_Click(object sender, RoutedEventArgs e)
    {

    }

    private void chbOnOffLeft_Click(object sender, RoutedEventArgs e)
    {
            if (cameron != null)
            {
                cameron.DisplayStatus[0] = (bool)chbOnOffLeft.IsChecked;
            }
        }

    private void chbOnOffRight_Click(object sender, RoutedEventArgs e)
    {
            if (cameron != null)
            {
                cameron.DisplayStatus[1] = (bool)chbOnOffRight.IsChecked;
            }
        }

    private void slBrightnessLeft_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {

    }

    private void slBrightnessRight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {

    }

    private void slStatusOn_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
            if (cameron != null) {
                cameron.LEDOnOff = new ushort[2] { (ushort)slStatusOn.Value, (ushort)slStatusOff.Value };
            }
        }

    private void slStatusOff_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
            if (cameron != null)
            {
                cameron.LEDOnOff = new ushort[2] { (ushort)slStatusOn.Value, (ushort)slStatusOff.Value };
            }
        }

    #endregion

    #region Calibration

    private void tabCalibration_Loaded(object sender, RoutedEventArgs e)
    {
      //Queries for all the camera calibration values
      bool calibrationPresent = false;
      DeviceLib.Wrapper.CameronDeviceLib.GetCameronCalibrationPresent(cameron.IntPtr, ref calibrationPresent);
      DeviceLib.Wrapper.Cameron_INTR intrinsics = new DeviceLib.Wrapper.Cameron_INTR();
      DeviceLib.Wrapper.CameronDeviceLib.GetCameronIntrinsics(cameron.IntPtr, ref intrinsics);
      DeviceLib.Wrapper.Cameron_EXTR extrinsics = new DeviceLib.Wrapper.Cameron_EXTR();
      DeviceLib.Wrapper.CameronDeviceLib.GetCameronExtrinsics(cameron.IntPtr, ref extrinsics);

      tbCurrentCameraCalibration.Text = intrinsics.k6.ToString() + ", " + extrinsics.translation[1];

      //Queries for all the IMU calibration values
      DeviceLib.Wrapper.CameronIMU_Calibration imu = new DeviceLib.Wrapper.CameronIMU_Calibration();
      DeviceLib.Wrapper.CameronDeviceLib.GetCameronIMUCalibration(cameron.IntPtr, ref imu);

      tbCurrentIMUCalibration.Text = imu.magnetometerDrift[2].ToString();

      //Queries for all the padding values
      DeviceLib.Wrapper.CameronDisplayCalibration displayCalibration = new DeviceLib.Wrapper.CameronDisplayCalibration();
      DeviceLib.Wrapper.CameronDeviceLib.GetCameronDisplayPadding(cameron.IntPtr, ref displayCalibration);

      tbLeftDisplayLeftPadding.Text = displayCalibration.paddingLeftDisplay[0].ToString();
      tbLeftDisplayTopPadding.Text = displayCalibration.paddingLeftDisplay[1].ToString();
      tbRightDisplayLeftPadding.Text = displayCalibration.paddingRightDisplay[0].ToString();
      tbRightDisplayTopPadding.Text = displayCalibration.paddingRightDisplay[1].ToString();

      //testing the other methods
      float[] fov = new float[2];
      float[] rectifiedfov = new float[2];
      DeviceLib.Wrapper.CameronDeviceLib.GetCameronFOV(cameron.IntPtr, fov);
      DeviceLib.Wrapper.CameronDeviceLib.GetCameronRectifiedFOV(cameron.IntPtr, rectifiedfov);

      //JUST FOR TESTING COMMUNICATION TO THE C++ LIB
      {
        DeviceLib.Wrapper.CameronDeviceLib.SetCameronIntrinsics(cameron.IntPtr, intrinsics);
        DeviceLib.Wrapper.CameronDeviceLib.SetCameronExtrinsics(cameron.IntPtr, extrinsics);
        DeviceLib.Wrapper.CameronDeviceLib.SetCameronIMUCalibration(cameron.IntPtr, imu);
        DeviceLib.Wrapper.CameronDeviceLib.SetCameronDisplayPadding(cameron.IntPtr, displayCalibration);
        DeviceLib.Wrapper.CameronDeviceLib.SetCameronFOV(cameron.IntPtr, fov);
        DeviceLib.Wrapper.CameronDeviceLib.SetCameronRectifiedFOV(cameron.IntPtr, rectifiedfov);
      }
    }

    private void bStartCameraCalibration_Click(object sender, RoutedEventArgs e)
    {

    }

    private void bSaveCameraCalibration_Click(object sender, RoutedEventArgs e)
    {

    }

    private void bStartIMUCalibration_Click(object sender, RoutedEventArgs e)
    {

    }

    private void bSaveIMUCalibration_Click(object sender, RoutedEventArgs e)
    {

    }

    private void bSaveDisplayPadding_Click(object sender, RoutedEventArgs e)
    {

    }

    #endregion

    #region FirmwareUpdate

    private void tabFirmware_Loaded(object sender, RoutedEventArgs e)
    {
      //Queries for all the values
      lbFirmwareVersionUpdate.Content = cameron.FirmwareVersion;
      lbFirmwareBuildUpdate.Content = cameron.FirmwareBuild;

      //JUST FOR TESTING COMMUNICATION TO THE C++ LIB
      {
        DeviceLib.Wrapper.CameronDeviceLib.UpdateFirmware("text.cameron.firmware");
      }
    }

    private void bBrowseFirmwareUpdate_Click(object sender, RoutedEventArgs e)
    {

    }

    private void bFirmwareUpdate_Click(object sender, RoutedEventArgs e)
    {
      DeviceLib.Wrapper.CameronDeviceLib.UpdateFirmware("text.cameron.firmware");
    }

    #endregion

  }

}
