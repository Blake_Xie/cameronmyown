﻿using System;
using System.Runtime.InteropServices;

namespace PCAR.Cameron.DeviceLib.Wrapper
{
  public delegate void CameronFrameReceivedHandler(CameronDevice sender, ref CameronFrame pFrameData);
  public delegate void CameronDeviceStatusChangedHandler(CameronDevice sender, bool isRunning);

  public class CameronDevice : IDisposable
  {

        #region Private Properties

        public static PCAR.Cameron.DeviceLib.Wrapper.WPF.CameronFrameViewer CFV;
        //public static DeviceLib.Wrapper.CameronIMUSample imu;

    private bool disposed = false;
    private GCHandle handle;
    private object cameronLock = new object();
    private IntPtr cameronInstance = IntPtr.Zero;
    private bool running = false;

    private static CameronFrameCallback cameronFrameCallback = new CameronFrameCallback(OnFrameCallback);

    private string deviceName = CameronDeviceLibInternal.UNKNOWN_STRING;
    private string serialNumber = CameronDeviceLibInternal.UNKNOWN_STRING;
    private string firmwareVersion = CameronDeviceLibInternal.UNKNOWN_STRING;
    private string firmwareBuild = CameronDeviceLibInternal.UNKNOWN_STRING;

    //camera
    private CameronResolutionInfo resolutionInfo = CameronDeviceLibInternal.CAMERON_DEFAULT_RESOLUTION_INFO;
    private float exposure = CameronDeviceLib.CAMERON_DEFAULT_EXPOSURE;
    private bool autoexposure = CameronDeviceLib.CAMERON_DEFAULT_AUTOEXPOSURE;
    private float gain = CameronDeviceLib.CAMERON_DEFAULT_GAIN;
    private bool hFlip = CameronDeviceLib.CAMERON_DEFAULT_HFLIP;
    private bool vFlip = CameronDeviceLib.CAMERON_DEFAULT_VFLIP;
    private bool undistort = CameronDeviceLib.CAMERON_DEFAULT_UNDISTORT;

    //displays
    private bool[] displayStatus = new bool[2];
    //modify by xrx 0825: uint->byte
    //0~2 : Left, R,G,B
    //3~5：Right R,G,B
    private byte[] displayBrightness = new byte[6];

        //modify by xrx 0825: float->ushort
        private ushort[] ledOnOff = new ushort[] { CameronDeviceLib.CAMERON_DEFAULT_LED_ON, CameronDeviceLib.CAMERON_DEFAULT_LED_OFF };

    #endregion

    #region Public Properties

    public IntPtr IntPtr
    {
      get { return cameronInstance; }
    }

    public static string LibVersion
    {
      get { return CameronDeviceLib.GetLibVersion(); }
    }

    public string DeviceName
    {
      get { return deviceName; }
    }

    public string SerialNumber
    {
      get { return serialNumber; }
    }

    public string FirmwareVersion
    {
      get { return firmwareVersion; }
    }

    public string FirmwareBuild
    {
      get { return firmwareBuild; }
    }

    public CameronResolutionInfo Resolution
    {
      set
      {
        resolutionInfo = value;
        if (cameronInstance != IntPtr.Zero)
          CameronDeviceLib.SetCameronResolutionInfo(cameronInstance, resolutionInfo);
      }
      get
      {
        return resolutionInfo;
      }
    }

    public int FrameWidth
    {
      get { return resolutionInfo.width; }
    }

    public int FrameHeight
    {
      get { return resolutionInfo.height; }
    }

    public float FPS
    {
      get { return resolutionInfo.fps; }
    }

    public bool Running
    {
      get { return running; }
    }

    public float Exposure
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronExposure(cameronInstance, value))
          exposure = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
          CameronDeviceLib.GetCameronExposure(cameronInstance, ref exposure);
        return exposure;
      }
    }

    public bool AutoExposure
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronAutoExposure(cameronInstance, value))
          autoexposure = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
          CameronDeviceLib.GetCameronAutoExposure(cameronInstance, ref autoexposure);
        return autoexposure;
      }
    }

    public float Gain
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronGain(cameronInstance, value))
          gain = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
          CameronDeviceLib.GetCameronGain(cameronInstance, ref gain);
        return gain;
      }
    }

    public bool HFlip
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronHFlip(cameronInstance, value))
          hFlip = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
          CameronDeviceLib.GetCameronHFlip(cameronInstance, ref hFlip);
        return hFlip;
      }
    }

    public bool VFlip
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronVFlip(cameronInstance, value))
          vFlip = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
          CameronDeviceLib.GetCameronVFlip(cameronInstance, ref vFlip);
        return vFlip;
      }
    }

    public bool Undistort
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronUndistort(cameronInstance, value))
          undistort = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
          CameronDeviceLib.GetCameronUndistort(cameronInstance, ref undistort);
        return undistort;
      }
    }

    public bool[] DisplayStatus
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronDisplayStatus(cameronInstance, value))
          displayStatus = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
        {
          CameronDeviceLib.GetCameronDisplayStatus(cameronInstance, displayStatus);
        }
        return displayStatus;
      }
    }

    public byte[] DisplayBrightness
    {
      set
      {
        if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronDisplayBrightness(cameronInstance, value))
          displayBrightness = value;
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
        {
          CameronDeviceLib.GetCameronDisplayBrightness(cameronInstance, displayBrightness);
        }
        return displayBrightness;
      }
    }

    public ushort[] LEDOnOff
    {
      set
      {
                if (cameronInstance != IntPtr.Zero && CameronDeviceLib.SetCameronStatusLED(cameronInstance, value))
                {
                    Console.WriteLine("Set LED");
                    ledOnOff = value;
                }
      }
      get
      {
        if (cameronInstance != IntPtr.Zero)
        {
          CameronDeviceLib.GetCameronStatusLED(cameronInstance, ledOnOff);
                    Console.WriteLine("Get LED");
                }
        return ledOnOff;
      }
    }

    #endregion

    #region .ctor

    public CameronDevice()
    {
      handle = GCHandle.Alloc(this);
      if (CameronDeviceLib.OpenCameron(out cameronInstance))
      {
                CameronDeviceLib.SetCameronResolutionInfo(cameronInstance, resolutionInfo);
                ///CameronDeviceLib.SetCameronHFlip(cameronInstance, hFlip);
                //CameronDeviceLib.SetCameronVFlip(cameronInstance, vFlip);
                CameronDeviceLib.SetCameronExposure(cameronInstance, exposure);
                CameronDeviceLib.SetCameronAutoExposure(cameronInstance, autoexposure);
                CameronDeviceLib.SetCameronGain(cameronInstance, gain);
                ushort[] abc = new ushort[2];
                CameronDeviceLib.GetCameronStatusLED(cameronInstance, abc);
                CameronDeviceLib.SetCameronStatusLED(cameronInstance, abc);
                CameronDeviceLib.SetCameronUndistort(cameronInstance, undistort);

                if (!CameronDeviceLib.GetCameronDeviceName(cameronInstance, ref deviceName))
          deviceName = CameronDeviceLibInternal.UNKNOWN_STRING;
                if (!CameronDeviceLib.GetCameronSerialNumber(cameronInstance, ref serialNumber))
                    deviceName = CameronDeviceLibInternal.UNKNOWN_STRING;
                if (!CameronDeviceLib.GetCameronFirmwareVersion(cameronInstance, ref firmwareVersion))
          deviceName = CameronDeviceLibInternal.UNKNOWN_STRING;
        if (!CameronDeviceLib.GetCameronFirmwareBuild(cameronInstance, ref firmwareBuild))
          deviceName = CameronDeviceLibInternal.UNKNOWN_STRING;
      }
    }

    ~CameronDevice()
    {
      // Finalizer calls Dispose(false)
      Dispose(false);
    }

    #endregion

    #region IDisposable

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected void Dispose(bool disposing)
    {
      if (!disposed)
      {
        if (disposing)
        {
          //dispose children objects
        }
        // free native resources if there are any.
        Destroy();
        disposed = true;
      }
      // free managed resources
      Stop();
    }

    private void Destroy()
    {
      lock (cameronLock)
      {
        if (cameronInstance == IntPtr.Zero)
          return;
        CameronDeviceLib.CloseCameron(cameronInstance);
        cameronInstance = IntPtr.Zero;
      }
    }

    #endregion

    #region Methods

    private static void OnFrameCallback(ref CameronFrame pFrameData, IntPtr pUserData)
    {
      if (pUserData == IntPtr.Zero)
        return;

      GCHandle gch = GCHandle.FromIntPtr(pUserData);
      CameronDevice device = gch.Target as CameronDevice;
      if (device == null)
        return;

      //CFV.CFVR(ref pFrameData);

      //imu = pFrameData.IMUData[0];
            
     device.OnFrameCallback(ref pFrameData);
        }

    private void OnFrameCallback(ref CameronFrame pFrameData)
    {
      if (CameronFrameReceived != null)
        CameronFrameReceived(this, ref pFrameData);
            
    }

    public bool Start()
    {
      lock (cameronLock)
      {
        if ((cameronInstance == IntPtr.Zero) || (running))
          return false;
                bool ret = CameronDeviceLib.StartCameron(cameronInstance, cameronFrameCallback, GCHandle.ToIntPtr(handle));
        if (ret)
        {
          running = true;
          if (CameronDeviceStatusChanged != null)
            CameronDeviceStatusChanged(this, true);
        }
        return ret;
      }
    }


        public bool Start(PCAR.Cameron.DeviceLib.Wrapper.WPF.CameronFrameViewer tempCFV)
        {
            lock (cameronLock)
            {
                if ((cameronInstance == IntPtr.Zero) || (running))
                    return false;

                CFV = tempCFV;

                bool ret = CameronDeviceLib.StartCameron(cameronInstance, cameronFrameCallback, GCHandle.ToIntPtr(handle));
                if (ret)
                {
                    running = true;
                    if (CameronDeviceStatusChanged != null)
                        CameronDeviceStatusChanged(this, true);
                }
                return ret;
            }
        }
        public bool Stop()
    {
      lock (cameronLock)
      {
        if ((cameronInstance == IntPtr.Zero) || (!running))
          return false;
        bool ret = CameronDeviceLib.StopCameron(cameronInstance);
        if (ret)
        {
          running = false;
          if (CameronDeviceStatusChanged != null)
            CameronDeviceStatusChanged(this, false);
        }
        return ret;
      }
    }

    #endregion

    #region ToString Logic

    private static string GetResolutionInfoString(CameronResolutionInfo resolutionInfo)
    {
      return String.Format("[Resolution Info]\nFrame Resolution [w/h] : {0} x {1}\nFramerate : {4}\n", resolutionInfo.width, resolutionInfo.height, resolutionInfo.fps);
    }

    private string GetDeviceDescriptionString()
    {
      if (cameronInstance == IntPtr.Zero)
        return CameronDeviceLibInternal.UNKNOWN_STRING;

      return String.Format("Device Name : {0}\nSerial Number : {1}\n" +
                            "Firmware Version : {2}\nFirmware Build : {3}\n" +
                            "Device Status: {4} \n" +
                            "Exposure [%] : {5}\nGain [%] : {6}\nLED On [%] : {7}\nLED Off [%] : {8}\n" +
                            "Camera Flip [h/v] : [{9}, {10}]\n",
                            DeviceName, SerialNumber, FirmwareVersion, FirmwareBuild, Running ? "Running" : "Not Running",
                            Exposure, Gain, LEDOnOff[0], LEDOnOff[1], HFlip ? "YES" : "NO", VFlip ? "YES" : "NO");
    }

    public override string ToString()
    {
      if (cameronInstance == IntPtr.Zero)
        return CameronDeviceLibInternal.UNKNOWN_STRING;
      return GetDeviceDescriptionString() + GetResolutionInfoString(resolutionInfo);
    }

    #endregion

    #region Events

    public event CameronFrameReceivedHandler CameronFrameReceived;
    public event CameronDeviceStatusChangedHandler CameronDeviceStatusChanged;

    #endregion

  }

}