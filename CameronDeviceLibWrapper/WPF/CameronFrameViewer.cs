﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;

namespace PCAR.Cameron.DeviceLib.Wrapper.WPF
{
  public class CameronFrameViewer : Grid, IDisposable
  {

    #region Depedency Properties

    public static readonly DependencyProperty FramerateProperty = DependencyProperty.Register("Framerate", typeof(double), typeof(CameronFrameViewer), new UIPropertyMetadata(60.0, new PropertyChangedCallback(FrameratePropertyChanged)));
    public double Framerate
    {
      get { return (double)GetValue(FramerateProperty); }
      set { SetValue(FramerateProperty, value); }
    }

    private static void FrameratePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
    {
      CameronFrameViewer instance = obj as CameronFrameViewer;
      if (instance == null)
        return;
      double newFramerate = (double)e.NewValue;
      lock (instance.updateLock)
      {
        if (instance.timer != null)
        {
          bool needRestart = instance.timer.IsEnabled;
          instance.timer.Stop();
          if (newFramerate < 1.0)
            newFramerate = 1.0;
          if (newFramerate > 120.0)
            newFramerate = 120.0;
          instance.timer.Interval = TimeSpan.FromMilliseconds(1000.0 / newFramerate);
          if (needRestart)
            instance.timer.Start();
        }
      }
    }

    #endregion

    #region Properties

    private bool disposed = false;
    DispatcherTimer timer = null;
    private object updateLock = new object();
    private bool updated = false;
    private bool needToStart = false;
    public CameronImage image = null;
    private CameronDevice device;

    public CameronDevice Device
    {
      set
      {
        if (device == value)
          return;
        SetDevice(value);
      }
      get
      {
        return device;
      }
    }

    #endregion

    #region .ctor

    public CameronFrameViewer()
    {
      ColumnDefinitions.Add(new ColumnDefinition());
      Loaded += CameronFrameViewer_Loaded;
    }

    void CameronFrameViewer_Loaded(object sender, RoutedEventArgs e)
    {
      image = new CameronImage();
      Children.Add(image);
      SetColumn(image, 0);
      if (device != null)
        image.SetBitmapResolution((uint)device.FrameWidth, (uint)device.FrameHeight);

      timer = new DispatcherTimer();

            Framerate = 20;
            double newFramerate = Framerate;
      if (newFramerate < 1.0)
        newFramerate = 1.0;
      if (newFramerate > 120.0)
        newFramerate = 120.0;
      timer.Interval = TimeSpan.FromMilliseconds(1000.0 / newFramerate);
      timer.Tick += CameronTimerUpdate;

      lock (updateLock)
      {
        if (needToStart)
          timer.Start();
        needToStart = false;
      }
    }

    ~CameronFrameViewer()
    {
      Dispose(false);
    }

    #endregion

    #region Methods

    private void Start(CameronDevice device)
    {
      if (timer != null)
        timer.Start();
      else
      {
        lock (updateLock)
        {
          needToStart = true;
        }
      }
    }

    private void Stop(CameronDevice device)
    {
      if (timer != null)
        timer.Stop();
    }

    private void SetDevice(CameronDevice newDevice)
    {
      if (device != null)
      {
        device.CameronFrameReceived -= CameronFrameReceivedHandler;
        device.CameronDeviceStatusChanged -= CameronDeviceStatusChangedHandler;
      }
      if (newDevice != null)
      {
        newDevice.CameronFrameReceived += CameronFrameReceivedHandler;
        newDevice.CameronDeviceStatusChanged += CameronDeviceStatusChangedHandler;
        if (image != null)
          image.SetBitmapResolution((uint)newDevice.FrameWidth, (uint)newDevice.FrameHeight);
      }
      else
      {
        if (image != null)
          image.SetBitmapResolution(0, 0);
      }
      device = newDevice;
    }

        public void CFVR(ref CameronFrame pFrameData)
        {
            
            lock (updateLock)
            {
                updated = true;
                if (image != null)
                    image.SetCameronImageData(pFrameData.cameraData);
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected void Dispose(bool disposing)
    {
      if (!disposed)
      {
        if (disposing)
        {
          if (image != null)
            image.Dispose();
        }
        disposed = true;
      }
    }

    #endregion



    #region Event Handlers

    private void CameronFrameReceivedHandler(CameronDevice sender, ref CameronFrame pFrameData)
    {

            
            lock (updateLock)
            {
                updated = true;
                if (image != null)
                    image.SetCameronImageData(pFrameData.cameraData);
            }
        }

    private void CameronDeviceStatusChangedHandler(CameronDevice sender, bool isRunning)
    {
      if (isRunning)
        Start(sender);
      else
        Stop(sender);
    }

    private void CameronTimerUpdate(object sender, EventArgs e)
    {
      lock (updateLock)
      {
        if (!updated)
          return;

        updated = false;
        if ((image != null) && (image.Bitmap != null))
          image.Bitmap.Invalidate();
      }
    }

    #endregion

  }

}


//namespace webcam
//{
//    ///  
//    ///  avicap  的摘要说明。
//    ///  
//    public class showVideo
//    {
//        //  showVideo  calls
//        [DllImport("avicap32.dll")] public static extern IntPtr capCreateCaptureWindowA(byte[] lpszWindowName, int dwStyle, int x, int y, int nWidth, int nHeight, IntPtr hWndParent, int nID);
//        [DllImport("avicap32.dll")] public static extern bool capGetDriverDescriptionA(short wDriver, byte[] lpszName, int cbName, byte[] lpszVer, int cbVer);
//        [DllImport("User32.dll")] public static extern bool SendMessage(IntPtr hWnd, int wMsg, bool wParam, int lParam);
//        [DllImport("User32.dll")] public static extern bool SendMessage(IntPtr hWnd, int wMsg, short wParam, int lParam);
//        [DllImport("User32.dll")] public static extern bool SendMessage(IntPtr hWnd, int wMsg, short wParam, FrameEventHandler lParam);
//        [DllImport("User32.dll")] public static extern bool SendMessage(IntPtr hWnd, int wMsg, int wParam, ref BITMAPINFO lParam);
//        [DllImport("User32.dll")] public static extern int SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
//        [DllImport("avicap32.dll")] public static extern int capGetVideoFormat(IntPtr hWnd, IntPtr psVideoFormat, int wSize);

//        //  constants
//        public const int WM_USER = 0x400;
//        public const int WS_CHILD = 0x40000000;
//        public const int WS_VISIBLE = 0x10000000;
//        public const int SWP_NOMOVE = 0x2;
//        public const int SWP_NOZORDER = 0x4;
//        public const int WM_CAP_DRIVER_CONNECT = WM_USER + 10;
//        public const int WM_CAP_DRIVER_DISCONNECT = WM_USER + 11;
//        public const int WM_CAP_SET_CALLBACK_FRAME = WM_USER + 5;
//        public const int WM_CAP_SET_PREVIEW = WM_USER + 50;
//        public const int WM_CAP_SET_PREVIEWRATE = WM_USER + 52;
//        public const int WM_CAP_SET_VIDEOFORMAT = WM_USER + 45;

//        //  Structures
//        [StructLayout(LayoutKind.Sequential)]
//        public struct VIDEOHDR
//        {
//            [MarshalAs(UnmanagedType.I4)] public int lpData;
//            [MarshalAs(UnmanagedType.I4)] public int dwBufferLength;
//            [MarshalAs(UnmanagedType.I4)] public int dwBytesUsed;
//            [MarshalAs(UnmanagedType.I4)] public int dwTimeCaptured;
//            [MarshalAs(UnmanagedType.I4)] public int dwUser;
//            [MarshalAs(UnmanagedType.I4)] public int dwFlags;
//            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)] public int[] dwReserved;
//        }

//        [structlayout(layoutkind.sequential)]
//        public struct bitmapinfoheader
//        {
//            [MarshalAs(UnmanagedType.I4)] public Int32 biSize;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biWidth;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biHeight;
//            [MarshalAs(UnmanagedType.I2)] public short biPlanes;
//            [MarshalAs(UnmanagedType.I2)] public short biBitCount;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biCompression;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biSizeImage;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biXPelsPerMeter;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biYPelsPerMeter;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biClrUsed;
//            [MarshalAs(UnmanagedType.I4)] public Int32 biClrImportant;
//        }

//        [structlayout(layoutkind.sequential)]
//        public struct bitmapinfo
//        {
//            [MarshalAs(UnmanagedType.Struct, SizeConst = 40)] public BITMAPINFOHEADER bmiHeader;
//            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)] public Int32[] bmiColors;
//        }

//        public delegate void FrameEventHandler(IntPtr lwnd, IntPtr lpVHdr);

//        //  Public  methods
//        public static object GetStructure(IntPtr ptr, valueType structure)
//        {
//            return Marshal.PtrToStructure(ptr, structure.GetType());
//        }

//        public static object GetStructure(int ptr, valueType structure)
//        {
//            return GetStructure(new IntPtr(ptr), structure);
//        }

//        public static void Copy(IntPtr ptr, byte[] data)
//        {
//            Marshal.Copy(ptr, data, 0, data.Length);
//        }

//        public static void Copy(int ptr, byte[] data)
//        {
//            Copy(new IntPtr(ptr), data);
//        }

//        public static int SizeOf(object structure)
//        {
//            return Marshal.SizeOf(structure);
//        }
//    }

//    //web  camera  class
//    public class WebCamera
//    {
//        //  Constructur
//        public WebCamera(IntPtr handle, int width, int height)
//        {
//            mControlPtr = handle;
//            mWidth = width;
//            mHeight = height;
//        }

//        //  delegate  for  frame  callback
//        public delegate void RecievedFrameEventHandler(byte[] data);
//        public event RecievedFrameEventHandler RecievedFrame;

//        private IntPtr lwndC;  //  Holds  the  unmanaged  handle  of  the  control
//        private IntPtr mControlPtr;  //  Holds  the  managed  pointer  of  the  control
//        private int mWidth;
//        private int mHeight;

//        private showVideo.FrameEventHandler mFrameEventHandler;  //  Delegate  instance  for  the  frame  callback  -  must  keep  alive!  gc  should  NOT  collect  it

//        //  Close  the  web  camera
//        public void CloseWebcam()
//        {
//            this.capDriverDisconnect(this.lwndC);
//        }

//        //  start  the  web  camera
//        public void StartWebCam()
//        {
//            byte[] lpszName = new byte[100];
//            byte[] lpszVer = new byte[100];

//            showVideo.capGetDriverDescriptionA(0, lpszName, 100, lpszVer, 100);
//            this.lwndC = showVideo.capCreateCaptureWindowA(lpszName, showVideo.WS_VISIBLE + showVideo.WS_CHILD, 0, 0, mWidth, mHeight, mControlPtr, 0);

//            if (this.capDriverConnect(this.lwndC, 0))
//            {
//                this.capPreviewRate(this.lwndC, 66);
//                this.capPreview(this.lwndC, true);
//                showVideo.BITMAPINFO bitmapinfo = new showVideo.BITMAPINFO();
//                bitmapinfo.bmiHeader.biSize = showVideo.SizeOf(bitmapinfo.bmiHeader);
//                bitmapinfo.bmiHeader.biWidth = 352;
//                bitmapinfo.bmiHeader.biHeight = 288;
//                bitmapinfo.bmiHeader.biPlanes = 1;
//                bitmapinfo.bmiHeader.biBitCount = 24;
//                this.capSetVideoFormat(this.lwndC, ref bitmapinfo, showVideo.SizeOf(bitmapinfo));
//                this.mFrameEventHandler = new showVideo.FrameEventHandler(FrameCallBack);
//                this.capSetCallbackOnFrame(this.lwndC, this.mFrameEventHandler);
//                showVideo.SetWindowPos(this.lwndC, 0, 0, 0, mWidth, mHeight, 6);
//            }
//        }

//        //  private  functions
//        private bool capDriverConnect(IntPtr lwnd, short i)
//        {
//            return showVideo.SendMessage(lwnd, showVideo.WM_CAP_DRIVER_CONNECT, i, 0);
//        }

//        private bool capdriverdisconnect(intptr lwnd)
//        {
//            return showVideo.SendMessage(lwnd, showVideo.WM_CAP_DRIVER_DISCONNECT, 0, 0);
//        }

//        private bool capPreview(IntPtr lwnd, bool f)
//        {
//            return showVideo.SendMessage(lwnd, showVideo.WM_CAP_SET_PREVIEW, f, 0);
//        }

//        private bool cappreviewrate(intptr lwnd, short wms)
//        {
//            return showVideo.SendMessage(lwnd, showVideo.WM_CAP_SET_PREVIEWRATE, wMS, 0);
//        }

//        private bool capSetCallbackOnFrame(IntPtr lwnd, showVideo.FrameEventHandler lpProc)
//        {
//            return showVideo.SendMessage(lwnd, showVideo.WM_CAP_SET_CALLBACK_FRAME, 0, lpProc);
//        }

//        private bool capsetvideoformat(intptr hcapwnd, ref showvideo.bitmapinfo bmpformat, int capformatsize)
//        {
//            return showVideo.SendMessage(hCapWnd, showVideo.WM_CAP_SET_VIDEOFORMAT, CapFormatSize, ref BmpFormat);
//        }

//        private void framecallback(intptr lwnd, intptr lpvhdr)
//        {
//            showVideo.VIDEOHDR videoHeader = new showVideo.VIDEOHDR();
//            byte[] VideoData;
//            videoHeader = (showVideo.VIDEOHDR)showVideo.GetStructure(lpVHdr, videoHeader);
//            VideoData = new byte[videoHeader.dwBytesUsed];
//            showVideo.Copy(videoHeader.lpData, VideoData);
//            if (this.RecievedFrame != null)
//                this.RecievedFrame(VideoData);
//        }
//    }

//}

