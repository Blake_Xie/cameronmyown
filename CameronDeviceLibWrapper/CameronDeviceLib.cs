﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace PCAR.Cameron.DeviceLib.Wrapper
{

  #region [ Cameron Structures ]

  // Cameron resolution info
  [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
  public struct CameronResolutionInfo
  {
    [MarshalAs(UnmanagedType.I4)]
    public int width;
    [MarshalAs(UnmanagedType.I4)]
    public int height;
    [MarshalAs(UnmanagedType.R4)]
    public float fps;
  };

  // Cameron Frame
  [StructLayout(LayoutKind.Sequential)]
  public struct CameronFrame
  {
    [MarshalAs(UnmanagedType.U4)]
    public uint width;                  // Cameron frame width
    [MarshalAs(UnmanagedType.U4)]
    public uint height;                 // Cameron frame height
    [MarshalAs(UnmanagedType.U4)]
    public uint timeStamp;              // Cameron frame time stamp in 100us increments
    [MarshalAs(UnmanagedType.U4)]
    public uint lightSensor;            // Cameron frame time stamp in 100us increments
    [MarshalAs(UnmanagedType.U4)]
    public uint proximitySensor;        // Cameron frame time stamp in 100us increments
    public IntPtr cameraData;           // Cameron left frame data
    [MarshalAs(UnmanagedType.U4)]
    public uint IMUSamplesCount;        // Number of IMU data samples in this frame
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100)]
    public CameronIMUSample[] IMUData;  // Cameron IMU samples
  }

  // Cameron IMU Sample
  [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
  public struct CameronIMUSample
  {
    [MarshalAs(UnmanagedType.U4)]
    public uint timeStamp;          // IMU time stamp in 100us increments
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] accelData;     // Cameron accelerometer data
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] gyroData;      // Cameron gyroscope data
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] magData;      // Cameron magnetometer data
  };

  // Cameron Intrinsics
  [StructLayout(LayoutKind.Sequential)]
  public struct Cameron_INTR
  {
    [MarshalAs(UnmanagedType.U4)]
    public uint width; //px
    [MarshalAs(UnmanagedType.U4)]
    public uint height; //px

    [MarshalAs(UnmanagedType.R4)]
    public float k1;
    [MarshalAs(UnmanagedType.R4)]
    public float k2;
    [MarshalAs(UnmanagedType.R4)]
    public float k3;
    [MarshalAs(UnmanagedType.R4)]
    public float k4;
    [MarshalAs(UnmanagedType.R4)]
    public float k5;
    [MarshalAs(UnmanagedType.R4)]
    public float k6;
    [MarshalAs(UnmanagedType.R4)]
    public float p1;
    [MarshalAs(UnmanagedType.R4)]
    public float p2;
    [MarshalAs(UnmanagedType.R4)]
    public float fx;
    [MarshalAs(UnmanagedType.R4)]
    public float fy;
    [MarshalAs(UnmanagedType.R4)]
    public float cx;
    [MarshalAs(UnmanagedType.R4)]
    public float cy;
  }

  // Cameron Extrinsics
  [StructLayout(LayoutKind.Sequential)]
  public struct Cameron_EXTR
  {
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] rotation;         // DUO camera rotation matrix
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] translation;        // DUO camera translation vector
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct CameronIMU_Calibration
  {
    [MarshalAs(UnmanagedType.U4)]
    public uint meanTimeDelta; //ms

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] gravityDirection;// unit vector
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] magnetometerDirection;// unit vector
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] accelerometerVariance;// g-units^2
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] accelerometerDrift;// g-units/s
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] gyroVariance;// (rad/s)^2
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] gyroDrift;// rad/s
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] magnetometerVariance;// units^2
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.R4)]
    public float[] magnetometerDrift;// units^2
  }


    //modify by xrx 0825: API change, int->byte
 [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
  public struct CameronDisplayCalibration
  {
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2, ArraySubType = UnmanagedType.U4)]
    public byte[] paddingLeftDisplay;      // Cameron left display padding data
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2, ArraySubType = UnmanagedType.U4)]
    public byte[] paddingRightDisplay;      // Cameron left display padding data
  }

    //Add by xrx 0825:API add new data structure
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct CameronInfo
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U1)]
        byte[] device_name;             //Cameron Name  MAX 56 bit
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U1)]
        byte[] device_version;          //Cameron Version
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U1)]
        byte[] serial_number;           /* Serial Number */
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U1)]
        byte[] manufacturer_string; /*manufacturer string*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U1)]
        byte[] product_string;                                    /* Product string */
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U1)]
        byte[] firmware_version;        /*FirmwareVersion*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U1)]
        byte[] firmware_build;        /*FirmwareBuild*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U2)]
        ushort led_on_time;        /*Led status (ms)*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U2)]
        ushort led_off_time ;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        bool display_left;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        bool display_right;
        /*Display Brightness*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        byte display_left_brightness_r;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        byte display_left_brightness_g;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        byte display_left_brightness_b;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        byte display_right_brightness_r;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        byte display_right_brightness_g;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1, ArraySubType = UnmanagedType.U1)]
        byte display_right_brightness_b;
        /*Display Padding*/
        CameronDisplayCalibration display_padding;
    }
    #endregion

    #region [ Cameron Callbacks ]

    public delegate void CameronFrameCallback(ref CameronFrame pFrameData, IntPtr pUserData);

  #endregion

  #region [ Cameron Library ]
  public static class CameronDeviceLib
  {

    #region [ Cameron Default Settings ]

    public const float CAMERON_DEFAULT_GAIN = 10.0f;
    public const float CAMERON_DEFAULT_EXPOSURE = 20.0f;
    public const bool CAMERON_DEFAULT_AUTOEXPOSURE = false;
    public const int CAMERON_DEFAULT_RESOLUTION_WIDTH = 320;
    public const int CAMERON_DEFAULT_RESOLUTION_HEIGHT = 180;
    public const float CAMERON_DEFAULT_RESOLUTION_FRAMERATE = 30.0f;
    public const bool CAMERON_DEFAULT_HFLIP = false;
    public const bool CAMERON_DEFAULT_VFLIP = false;
    public const bool CAMERON_DEFAULT_UNDISTORT = false;

    public const ushort CAMERON_DEFAULT_LED_ON = 100;
    public const ushort CAMERON_DEFAULT_LED_OFF = 110;

    #endregion

    #region [ Cameron Library Generic Logic ]

    public static string GetLibVersion()
    {
      string libVersionString = null;
      IntPtr rawPtr = CameronDeviceLibInternal.GetCameronDeviceLibVersion();
      if (rawPtr != IntPtr.Zero)
      {
        try { libVersionString = Marshal.PtrToStringAnsi(rawPtr); }
        catch { }
      }
      return libVersionString;
    }

    #endregion

    #region [ Cameron Device Initialization ]

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "OpenCameron")]
    public static extern bool OpenCameron(out IntPtr cameron);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "CloseCameron")]
    public static extern bool CloseCameron(IntPtr cameron);

    #endregion

    #region [ Cameron Device Capture Control ]

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "StartCameron")]
    public static extern bool StartCameron(IntPtr cameron, CameronFrameCallback frameCallback, IntPtr pUserData);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "StopCameron")]
    public static extern bool StopCameron(IntPtr cameron);

    #endregion

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "UpdateFirmware")]
    public static extern bool UpdateFirmware(string filename);

        #region [ Cameron Get Parameters ]
        //modify by xrx 0825: string max is 56
        //modify by xrx 0825:StatusLED set and get float->ushort[]
        //modify by xrx 0825:DisplayBrightness set and get int[]->byte[]
        public static bool GetCameronDeviceName(IntPtr cameron, ref string val)
    {
      try
      {
        //UnmanagedString str = new UnmanagedString();
        StringBuilder str = new StringBuilder(56);
        if (CameronDeviceLibInternal.GetCameronDeviceName(cameron, str, str.Capacity))
        {
          val = str.ToString();
          return true;
        }
        else return false;
      }
      catch { return false; }
    }

    public static bool GetCameronSerialNumber(IntPtr cameron, ref string val)
    {
      try
      {
        StringBuilder str = new StringBuilder(56);
        if (CameronDeviceLibInternal.GetCameronSerialNumber(cameron, str, str.Capacity))
        {
          val = str.ToString();
          return true;
        }
        else return false;
      }
      catch { return false; }
    }

    public static bool GetCameronFirmwareVersion(IntPtr cameron, ref string val)
    {
      try
      {
        StringBuilder str = new StringBuilder(56);
        if (CameronDeviceLibInternal.GetCameronFirmwareVersion(cameron, str, str.Capacity))
        {
          val = str.ToString();
          return true;
        }
        else return false;
      }
      catch { return false; }
    }

    public static bool GetCameronFirmwareBuild(IntPtr cameron, ref string val)
    {
      try
      {
        StringBuilder str = new StringBuilder(56);
        if (CameronDeviceLibInternal.GetCameronFirmwareBuild(cameron, str, str.Capacity))
        {
          val = str.ToString();
          return true;
        }
        else return false;
      }
      catch { return false; }
    }

    public static bool GetCameronResolutionInfo(IntPtr cameron, ref CameronResolutionInfo val)
    {
      return CameronDeviceLibInternal.GetCameronResolutionInfo(cameron, ref val);
    }

    public static bool GetCameronExposure(IntPtr cameron, ref float val)
    {
      return CameronDeviceLibInternal.GetCameronExposure(cameron, ref val);
    }

    public static bool GetCameronExposureMS(IntPtr cameron, ref float val)
    {
      return CameronDeviceLibInternal.GetCameronExposureMS(cameron, ref val);
    }

    public static bool GetCameronAutoExposure(IntPtr cameron, ref bool val)
    {
      return CameronDeviceLibInternal.GetCameronAutoExposure(cameron, ref val);
    }

    public static bool GetCameronGain(IntPtr cameron, ref float val)
    {
      return CameronDeviceLibInternal.GetCameronGain(cameron, ref val);
    }

    public static bool GetCameronHFlip(IntPtr cameron, ref bool val)
    {
      return CameronDeviceLibInternal.GetCameronHFlip(cameron, ref val);
    }

    public static bool GetCameronVFlip(IntPtr cameron, ref bool val)
    {
      return CameronDeviceLibInternal.GetCameronVFlip(cameron, ref val);
    }

    public static bool GetCameronCalibrationPresent(IntPtr cameron, ref bool val)
    {
      return CameronDeviceLibInternal.GetCameronCalibrationPresent(cameron, ref val);
    }

    public static bool GetCameronFOV(IntPtr cameron, float[] val)
    {
      return CameronDeviceLibInternal.GetCameronFOV(cameron, val);
    }

    public static bool GetCameronRectifiedFOV(IntPtr cameron, float[] val)
    {
      return CameronDeviceLibInternal.GetCameronRectifiedFOV(cameron, val);
    }

    public static bool GetCameronUndistort(IntPtr cameron, ref bool val)
    {
      return CameronDeviceLibInternal.GetCameronUndistort(cameron, ref val);
    }

    public static bool GetCameronIntrinsics(IntPtr cameron, ref Cameron_INTR val)
    {
      return CameronDeviceLibInternal.GetCameronIntrinsics(cameron, ref val);
    }

    public static bool GetCameronExtrinsics(IntPtr cameron, ref Cameron_EXTR val)
    {
      return CameronDeviceLibInternal.GetCameronExtrinsics(cameron, ref val);
    }

    public static bool GetCameronDisplayStatus(IntPtr cameron, bool[] val)
    {
      return CameronDeviceLibInternal.GetCameronDisplayStatus(cameron, val);
    }

    public static bool GetCameronDisplayBrightness(IntPtr cameron, byte[] val)
    {
      return CameronDeviceLibInternal.GetCameronDisplayBrightness(cameron, val);
    }

    public static bool GetCameronStatusLED(IntPtr cameron, ushort[] ledOnOff)
    {
      return CameronDeviceLibInternal.GetCameronStatusLED(cameron, ledOnOff); ;
    }

    public static bool GetCameronDisplayPadding(IntPtr cameron, ref CameronDisplayCalibration val)
    {
      return CameronDeviceLibInternal.GetCameronDisplayPadding(cameron, ref val);
    }

    public static bool GetCameronIMUCalibration(IntPtr cameron, ref CameronIMU_Calibration val)
    {
      return CameronDeviceLibInternal.GetCameronIMUCalibration(cameron, ref val);
    }

    #endregion

    #region [ Cameron Set Parameters ]

    public static bool SetCameronResolutionInfo(IntPtr cameron, CameronResolutionInfo val)
    {
      return CameronDeviceLibInternal.SetCameronResolutionInfo(cameron, val);
    }

    public static bool SetCameronExposure(IntPtr cameron, float val)
    {
      return CameronDeviceLibInternal.SetCameronExposure(cameron, val);
    }

    public static bool SetCameronExposureMS(IntPtr cameron, float val)
    {
      return CameronDeviceLibInternal.SetCameronExposureMS(cameron, val);
    }

    public static bool SetCameronAutoExposure(IntPtr cameron, bool val)
    {
      return CameronDeviceLibInternal.SetCameronAutoExposure(cameron, val);
    }

    public static bool SetCameronGain(IntPtr cameron, float val)
    {
      return CameronDeviceLibInternal.SetCameronGain(cameron, val);
    }

    public static bool SetCameronHFlip(IntPtr cameron, bool val)
    {
      return CameronDeviceLibInternal.SetCameronHFlip(cameron, val);
    }

    public static bool SetCameronVFlip(IntPtr cameron, bool val)
    {
      return CameronDeviceLibInternal.SetCameronVFlip(cameron, val);
    }

    public static bool SetCameronUndistort(IntPtr cameron, bool val)
    {
      return CameronDeviceLibInternal.SetCameronUndistort(cameron, val);
    }

    public static bool SetCameronDeviceName(IntPtr cameron, string val)
    {
      return CameronDeviceLibInternal.SetCameronDeviceName(cameron, val);
    }

    public static bool SetCameronSerialNumber(IntPtr cameron, string val)
    {
      return CameronDeviceLibInternal.SetCameronSerialNumber(cameron, val);
    }

    public static bool SetCameronFOV(IntPtr cameron, float[] val)
    {
      return CameronDeviceLibInternal.SetCameronFOV(cameron, val);
    }

    public static bool SetCameronRectifiedFOV(IntPtr cameron, float[] val)
    {
      return CameronDeviceLibInternal.SetCameronRectifiedFOV(cameron, val);
    }

    public static bool SetCameronIntrinsics(IntPtr cameron, Cameron_INTR val)
    {
      return CameronDeviceLibInternal.SetCameronIntrinsics(cameron, val);
    }

    public static bool SetCameronExtrinsics(IntPtr cameron, Cameron_EXTR val)
    {
      return CameronDeviceLibInternal.SetCameronExtrinsics(cameron, val);
    }

    public static bool SetCameronStatusLED(IntPtr cameron, ushort[] ledOnOff)
    {
      return CameronDeviceLibInternal.SetCameronStatusLED(cameron, ledOnOff);
    }

    public static bool SetCameronDisplayStatus(IntPtr cameron, bool[] val)
    {
      return CameronDeviceLibInternal.SetCameronDisplayStatus(cameron, val);
    }

    public static bool SetCameronDisplayBrightness(IntPtr cameron, byte[] val)
    {
      return CameronDeviceLibInternal.SetCameronDisplayBrightness(cameron, val);
    }

    public static bool SetCameronDisplayPadding(IntPtr cameron, CameronDisplayCalibration val)
    {
      return CameronDeviceLibInternal.SetCameronDisplayPadding(cameron, val);
    }

    public static bool SetCameronIMUCalibration(IntPtr cameron, CameronIMU_Calibration val)
    {
      return CameronDeviceLibInternal.SetCameronIMUCalibration(cameron, val);
    }

    #endregion

  }

  #endregion

}