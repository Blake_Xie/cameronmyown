﻿using System;
using System.Runtime.InteropServices;
using PCAR.Cameron.DeviceLib.Wrapper;
using System.Text;

namespace PCAR.Cameron.DeviceLib.Wrapper
{

  internal static class CameronDeviceLibInternal
  {
    #region [ Cameron internal const]

    internal const int CL_STRING_MAX_LENGTH = 252;
    internal const string UNKNOWN_STRING = "Unknown";
    internal static CameronResolutionInfo CAMERON_DEFAULT_RESOLUTION_INFO
    {
      get
      {
        CameronResolutionInfo res = new CameronResolutionInfo();
        GetCameronResolutionInfo(IntPtr.Zero, ref res);
        return res;
      }
    }

    #endregion

    #region [ Cameron library imported generic logic ]

    [DllImport("CameronDeviceLib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronDeviceLibVersion")]
    internal static extern IntPtr GetCameronDeviceLibVersion();

    #endregion

    #region [ Cameron Set/Get imported logic ]

    // Get
    [DllImport("CameronDeviceLib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronDeviceName")]
    internal static extern bool GetCameronDeviceName(IntPtr cameron, StringBuilder val, int len);
    [DllImport("CameronDeviceLib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronSerialNumber")]
    internal static extern bool GetCameronSerialNumber(IntPtr cameron, StringBuilder val, int len);
    [DllImport("CameronDeviceLib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronFirmwareVersion")]
    internal static extern bool GetCameronFirmwareVersion(IntPtr cameron, StringBuilder val, int len);
    [DllImport("CameronDeviceLib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronFirmwareBuild")]
    internal static extern bool GetCameronFirmwareBuild(IntPtr cameron, StringBuilder val, int len);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronResolutionInfo")]
    internal static extern bool GetCameronResolutionInfo(IntPtr cameron, ref CameronResolutionInfo val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronExposure")]
    internal static extern bool GetCameronExposure(IntPtr cameron, ref float val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronExposureMS")]
    internal static extern bool GetCameronExposureMS(IntPtr cameron, ref float val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronAutoExposure")]
    internal static extern bool GetCameronAutoExposure(IntPtr cameron, ref bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronGain")]
    internal static extern bool GetCameronGain(IntPtr cameron, ref float val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronHFlip")]
    internal static extern bool GetCameronHFlip(IntPtr cameron, ref bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronVFlip")]
    internal static extern bool GetCameronVFlip(IntPtr cameron, ref bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronCalibrationPresent")]
    internal static extern bool GetCameronCalibrationPresent(IntPtr cameron, ref bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronFOV")]
    internal static extern bool GetCameronFOV(IntPtr cameron, [In, Out] float[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronRectifiedFOV")]
    internal static extern bool GetCameronRectifiedFOV(IntPtr cameron, [In, Out] float[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronUndistort")]
    internal static extern bool GetCameronUndistort(IntPtr cameron, ref bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronIntrinsics")]
    internal static extern bool GetCameronIntrinsics(IntPtr cameron, ref Cameron_INTR val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronExtrinsics")]
    internal static extern bool GetCameronExtrinsics(IntPtr cameron, ref Cameron_EXTR val);

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronStatusLED")]
    internal static extern bool GetCameronStatusLED(IntPtr cameron, [In, Out] ushort[] val);

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronDisplayStatus")]
    internal static extern bool GetCameronDisplayStatus(IntPtr cameron,
      [MarshalAs(UnmanagedType.LPArray, ArraySubType=UnmanagedType.I1)]
      [In, Out] bool[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronDisplayBrightness")]
    internal static extern bool GetCameronDisplayBrightness(IntPtr cameron, [In, Out] byte[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronDisplayPadding")]
    internal static extern bool GetCameronDisplayPadding(IntPtr cameron, ref CameronDisplayCalibration val);

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "GetCameronIMUCalibration")]
    internal static extern bool GetCameronIMUCalibration(IntPtr cameron, ref CameronIMU_Calibration val);

    // Set
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronResolutionInfo")]
    internal static extern bool SetCameronResolutionInfo(IntPtr cameron, CameronResolutionInfo val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronExposure")]
    internal static extern bool SetCameronExposure(IntPtr cameron, float val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronExposureMS")]
    internal static extern bool SetCameronExposureMS(IntPtr cameron, float val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronAutoExposure")]
    internal static extern bool SetCameronAutoExposure(IntPtr cameron, bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronGain")]
    internal static extern bool SetCameronGain(IntPtr cameron, float val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronHFlip")]
    internal static extern bool SetCameronHFlip(IntPtr cameron, bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronVFlip")]
    internal static extern bool SetCameronVFlip(IntPtr cameron, bool val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronUndistort")]
    internal static extern bool SetCameronUndistort(IntPtr cameron, bool val);

    [DllImport("CameronDeviceLib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronDeviceName")]
    internal static extern bool SetCameronDeviceName(IntPtr cameron, string val);
    [DllImport("CameronDeviceLib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronSerialNumber")]
    internal static extern bool SetCameronSerialNumber(IntPtr cameron, string val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronFOV")]
    internal static extern bool SetCameronFOV(IntPtr cameron, float[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronRectifiedFOV")]
    internal static extern bool SetCameronRectifiedFOV(IntPtr cameron, float[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronIntrinsics")]
    internal static extern bool SetCameronIntrinsics(IntPtr cameron, Cameron_INTR val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronExtrinsics")]
    internal static extern bool SetCameronExtrinsics(IntPtr cameron, Cameron_EXTR val);

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronStatusLED")]
    internal static extern bool SetCameronStatusLED(IntPtr cameron, ushort[] val);

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronDisplayStatus")]
    internal static extern bool SetCameronDisplayStatus(IntPtr cameron,
      [MarshalAs(UnmanagedType.LPArray, ArraySubType=UnmanagedType.I1)]
      bool[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronDisplayBrightness")]
    internal static extern bool SetCameronDisplayBrightness(IntPtr cameron, byte[] val);
    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronDisplayPadding")]
    internal static extern bool SetCameronDisplayPadding(IntPtr cameron, CameronDisplayCalibration val);

    [DllImport("CameronDeviceLib", CallingConvention = CallingConvention.Cdecl, EntryPoint = "SetCameronIMUCalibration")]
    internal static extern bool SetCameronIMUCalibration(IntPtr cameron, CameronIMU_Calibration val);

    #endregion

  }

}
